import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tabs/constants/colors.dart';
import 'package:tabs/models/tab.dart';

showTabBottomSheet(BuildContext context, TabCardDetails tab) {
  showModalBottomSheet(
      backgroundColor: AppColors.white,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('${tab.name} Tab'),
              const Text('2021/10/22'),
              const SizedBox(
                height: 12,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (tab.description != null)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('Description'),
                        Text(tab.description ?? '')
                      ],
                    ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Amount'),
                      Text(tab.amount.toString())
                    ],
                  ),
                ],
              ),
              Expanded(child: SvgPicture.asset('assets/images/Finances.svg')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  TextButton(
                      onPressed: () {}, child: const Text('Change Amount')),
                  ElevatedButton(
                      onPressed: () {}, child: const Text('Close Tab'))
                ],
              )
            ],
          ),
        );
      });
}
