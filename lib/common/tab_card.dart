import 'package:flutter/material.dart';
import 'package:tabs/common/bottom_sheet.dart';
import 'package:tabs/constants/colors.dart';
import 'package:tabs/models/tab.dart';

class TabCard extends StatelessWidget {
  final TabCardDetails tab;

  const TabCard({Key? key, required this.tab}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 180,
      height: 180,
      child: GestureDetector(
        onTap: () {
          showTabBottomSheet(context, tab);
        },
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tab.name,
                  style: const TextStyle(fontWeight: FontWeight.w500),
                ),
                Text(
                  tab.amount.toString(),
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                ),
                if (tab.description != null)
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 4,
                    ),
                    clipBehavior: Clip.hardEdge,
                    constraints: const BoxConstraints(
                        maxWidth: 90,
                        minWidth: 30,
                        maxHeight: 30,
                        minHeight: 30),
                    decoration: BoxDecoration(
                        color: AppColors.fadedprimary,
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(
                      tab.description ?? "",
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(color: AppColors.primary),
                    ),
                  ),
                const Spacer(),
                const Text(
                  'a minute ago',
                  style: TextStyle(fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
