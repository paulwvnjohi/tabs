import 'package:flutter/material.dart';
import 'package:tabs/screens/settings.dart';
import 'package:tabs/screens/home/home.dart';
import 'package:tabs/screens/newtab/new_tab.dart';
import 'package:tabs/screens/signin.dart';

class AppRoutes {
  static String home = '/';
  static String signin = 'signin';
  static String newTab = 'newTab';
  static String settings = 'settings';

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(
        settings: settings,
        builder: (_) {
          switch (settings.name) {
            case 'home':
              return const Home();
            case 'signin':
              return const SignIn();
            case 'newTab':
              return const NewTab();
            case 'settings':
              return const Settings();

            default:
              return const Center(
                child: Text('404'),
              );
          }
        });
  }
}
