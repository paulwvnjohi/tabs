import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xFF1cc29f);
  static const fadedprimary = Color(0xFFdcf7ef);
  static const black100 = Color(0xFF333333);
  static const white = Color(0xFFfefefe);
  static const black = Color(0xFF000000);
  static const grey100 = Color(0XFF666666);
  //#4cae50 => indicator
}
