// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tab.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TabCardDetails _$TabCardDetailsFromJson(Map<String, dynamic> json) =>
    TabCardDetails(
      name: json['name'] as String,
      amount: (json['amount'] as num).toDouble(),
      description: json['description'] as String?,
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      isSettled: json['isSettled'] as bool?,
      settledAt: json['settledAt'] == null
          ? null
          : DateTime.parse(json['settledAt'] as String),
    );

Map<String, dynamic> _$TabCardDetailsToJson(TabCardDetails instance) =>
    <String, dynamic>{
      'name': instance.name,
      'amount': instance.amount,
      'description': instance.description,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'isSettled': instance.isSettled,
      'settledAt': instance.settledAt?.toIso8601String(),
    };
