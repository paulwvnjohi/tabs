import 'package:json_annotation/json_annotation.dart';

part 'tab.g.dart';

@JsonSerializable()
class TabCardDetails {
  String name;
  double amount;
  String? description;
  DateTime? createdAt;
  DateTime? updatedAt = DateTime.now();
  bool? isSettled = false;
  DateTime? settledAt;

  TabCardDetails(
      {required this.name,
      required this.amount,
      this.description,
      this.createdAt,
      this.updatedAt,
      this.isSettled,
      this.settledAt});

  factory TabCardDetails.fromJson(Map<String, dynamic> json) =>
      _$TabCardDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$TabCardDetailsToJson(this);
}
