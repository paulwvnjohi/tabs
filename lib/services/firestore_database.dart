import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tabs/models/tab.dart';

final usersRef = FirebaseFirestore.instance.collection('tabs');

class FireStore {
  @visibleForTesting
  List<TabCardDetails> mapQuerySnapshotToListNode(
    QuerySnapshot<Map<String, dynamic>> snapshot,
  ) {
    return snapshot.docs
        .map((json) => TabCardDetails.fromJson(json.data()))
        .toList();
  }

  Future<List<TabCardDetails>> getAllTabs() async {
    final querySnapShot = await usersRef.get();

    return querySnapShot.docs
        .map((json) => TabCardDetails.fromJson(json.data()))
        .toList();
  }

  Stream getSettledTabs({bool isSettled = true}) {
    return usersRef
        .where('isSettled', isEqualTo: isSettled)
        .snapshots()
        .map(mapQuerySnapshotToListNode);
  }
}

final settledTabsProvider =
    StreamProvider((ref) => FireStore().getSettledTabs());
final unSettledTabsProvider =
    StreamProvider((ref) => FireStore().getSettledTabs(isSettled: false));
