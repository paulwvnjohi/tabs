import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tabs/common/tab_card.dart';
import 'package:tabs/models/tab.dart';
import 'package:tabs/services/firestore_database.dart';

class OpenTabs extends ConsumerWidget {
  const OpenTabs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final streamAsyncValue = ref.watch(unSettledTabsProvider);

    return streamAsyncValue.when(
        data: (data) {
          if (data.length == 0) {
            return Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 300,
                    child: SvgPicture.asset(
                      "assets/images/List Is Empty.svg",
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  const Text("You don't have any open tabs")
                ],
              ),
            );
          }
          return Wrap(runSpacing: 10, spacing: 10, children: [
            ...data.map((TabCardDetails tab) => TabCard(tab: tab))
          ]);
        },
        error: (error, stack) =>
            Text('Oops, something unexpected happened: $error'),
        loading: () => const CircularProgressIndicator());
  }
}
