import 'package:flutter/material.dart';

import 'package:tabs/routes.dart';

import 'package:tabs/screens/home/closed_tabs.dart';
import 'package:tabs/screens/home/open_tabs.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            header(
              context,
              indicators: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _buildIndicators(currentIndex: currentIndex)),
            ),
            Expanded(
              child: PageView(
                controller: _pageController,
                onPageChanged: (int index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
                children: <Widget>[
                  Column(
                    children: [
                      OutlinedButton(
                          onPressed: () {}, child: const Text('close all')),
                      const SizedBox(
                        height: 5,
                      ),
                      const OpenTabs(),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: OutlinedButton(
                            onPressed: () {}, child: const Text('delete all')),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      const ClosedTabs(),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, AppRoutes.newTab);
        },
      ),
    );
  }
}

Widget header(BuildContext context, {required Widget indicators}) {
  Size size = MediaQuery.of(context).size;
  return Padding(
    padding: const EdgeInsets.only(left: 15, right: 15),
    child: SizedBox(
        height: 60,
        width: size.width,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
                top: 0,
                right: 0,
                child: IconButton(
                    onPressed: () =>
                        Navigator.pushNamed(context, AppRoutes.settings),
                    icon: const Icon(Icons.settings))),
            Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
              const Text('5 tabs open'),
              const Text(" 40000"),
              const SizedBox(
                height: 12,
              ),
              indicators,
            ]),
          ],
        )),
  );
}

Widget _indicator(bool isActive) {
  return AnimatedContainer(
    duration: const Duration(milliseconds: 300),
    width: isActive ? 30 : 6,
    height: 6,
    margin: const EdgeInsets.only(right: 5),
    decoration: BoxDecoration(
        color: Colors.grey, borderRadius: BorderRadius.circular(5)),
  );
}

List<Widget> _buildIndicators({required int currentIndex}) {
  List<Widget> indicators = [];
  for (var i = 0; i < 2; i++) {
    if (currentIndex == i) {
      indicators.add(_indicator(true));
    } else {
      indicators.add(_indicator(false));
    }
  }
  return indicators;
}
