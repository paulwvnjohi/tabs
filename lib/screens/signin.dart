import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SignIn extends StatelessWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: const CustomAppBar(),
        body: Stack(
          alignment: Alignment.center,
          children: [
            SvgPicture.asset(
              'assets/images/wave-haikei.svg',
              fit: BoxFit.fill,
            ),
            Positioned(
                bottom: 180,
                child: Column(
                  children: [
                    SizedBox(
                      width: deviceSize.width - 70,
                      child: OutlinedButton(
                          onPressed: () {},
                          style:
                              OutlinedButton.styleFrom(primary: Colors.black87),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 18,
                                height: 20,
                                child: SvgPicture.asset(
                                  'assets/images/google-logo.svg',
                                ),
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              const Text('Continue with Google')
                            ],
                          )),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    SizedBox(
                      width: deviceSize.width - 70,
                      child: ElevatedButton(
                          onPressed: () {},
                          style:
                              ElevatedButton.styleFrom(primary: Colors.black),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 18,
                                height: 20,
                                child: SvgPicture.asset(
                                  'assets/images/apple-logo.svg',
                                  color: Colors.white,
                                ),
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              const Text('Continue with apple'),
                            ],
                          )),
                    )
                  ],
                ))
          ],
        ));
  }
}

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
  })  : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFF1cc29f),
    );
  }
}
