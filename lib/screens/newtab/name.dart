import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:tabs/constants/colors.dart';

class Name extends StatefulWidget {
  const Name({Key? key}) : super(key: key);

  @override
  State<Name> createState() => _NameState();
}

class _NameState extends State<Name> {
  final TextEditingController _controller = TextEditingController();
  late List<Contact> contacts = [];
  searchContact(String name) async {
    List<Contact> foundContacts =
        await ContactsService.getContacts(query: name);
    setState(() {
      contacts = foundContacts;
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Name',
          style: Theme.of(context).textTheme.headline1,
        ),
        Text(
          "Enter the name of the person who you're making this tab for",
          style: Theme.of(context).textTheme.subtitle1,
        ),
        const SizedBox(
          height: 35,
        ),
        TextField(
          controller: _controller,
          autofocus: true,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.person, color: AppColors.primary),
            border: OutlineInputBorder(),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.primary, width: 1.0),
            ),
          ),
          onChanged: (value) {
            searchContact(value);
          },
        ),
        ConstrainedBox(
          constraints: const BoxConstraints(maxHeight: 400),
          child: ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: contacts.length,
              itemBuilder: (BuildContext context, int index) {
                return Center(
                    child: Text('Entry ${contacts[index].displayName}'));
              }),
        )
      ],
    );
  }
}
