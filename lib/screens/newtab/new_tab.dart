import 'package:flutter/material.dart';

import 'package:tabs/constants/colors.dart';
import 'package:tabs/screens/newtab/amount.dart';
import 'package:tabs/screens/newtab/name.dart';
import 'package:tabs/screens/newtab/reason.dart';

class NewTab extends StatefulWidget {
  const NewTab({Key? key}) : super(key: key);

  @override
  State<NewTab> createState() => _NewTabState();
}

class _NewTabState extends State<NewTab> {
  int pageNumber = 0;
  void _incrementPageNumber() {
    if (pageNumber < 2) {
      setState(() {
        pageNumber++;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    Duration _progressAnim = const Duration(milliseconds: 400);
    Gradient _defaultAppBarGradient = const LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          Color(0xFFd3d3d3),
          Color(0xFFe3e3e3),
          Color(0xFFededed),
        ]);
    Gradient _progressGradient =
        const LinearGradient(colors: [AppColors.primary, AppColors.primary]);
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(70.0),
          child: Column(
            children: [
              AppBar(
                backgroundColor: AppColors.white,
                foregroundColor: AppColors.black,
                title: const Text('New Tab'),
              ),
              SizedBox(
                height: 4,
                child: Row(
                  children: [
                    Container(
                        width: _screenSize.width / 3, color: AppColors.primary),
                    AnimatedContainer(
                      duration: _progressAnim,
                      width: _screenSize.width / 3,
                      decoration: BoxDecoration(
                          gradient: pageNumber > 0
                              ? _progressGradient
                              : _defaultAppBarGradient),
                    ),
                    AnimatedContainer(
                      duration: _progressAnim,
                      width: _screenSize.width / 3,
                      decoration: BoxDecoration(
                          gradient: pageNumber > 1
                              ? _progressGradient
                              : _defaultAppBarGradient),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        body: Padding(
          padding:
              const EdgeInsets.only(top: 25, bottom: 30, left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (pageNumber == 0) const Name(),
              if (pageNumber == 1) const Amount(),
              if (pageNumber == 2) const Reason(),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(onPressed: () {}, child: const Text('Cancel')),
                    OutlinedButton(
                        onPressed: () => _incrementPageNumber(),
                        child: const Text('Next')),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
