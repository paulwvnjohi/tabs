import 'package:flutter/material.dart';
import 'package:tabs/constants/colors.dart';

class Amount extends StatefulWidget {
  const Amount({Key? key}) : super(key: key);

  @override
  State<Amount> createState() => _AmountState();
}

class _AmountState extends State<Amount> {
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Amount",
          style: Theme.of(context).textTheme.headline1,
        ),
        Text(
          "Enter the amount Allan Mwaniki owes you",
          style: Theme.of(context).textTheme.subtitle1,
        ),
        const SizedBox(
          height: 35,
        ),
        TextField(
          controller: _controller,
          autofocus: true,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.message, color: AppColors.primary),
            border: OutlineInputBorder(),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.primary, width: 1.0),
            ),
          ),
          onChanged: (value) {
            print(value);
          },
        ),
      ],
    );
  }
}
