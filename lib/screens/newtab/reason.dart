import 'package:flutter/material.dart';
import 'package:tabs/constants/colors.dart';

class Reason extends StatefulWidget {
  const Reason({Key? key}) : super(key: key);

  @override
  State<Reason> createState() => _ReasonState();
}

class _ReasonState extends State<Reason> {
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "What's this for?",
          style: Theme.of(context).textTheme.headline1,
        ),
        Text(
          "Why does Paul Wanjohi owe you 50?",
          style: Theme.of(context).textTheme.subtitle1,
        ),
        const SizedBox(
          height: 35,
        ),
        TextField(
          controller: _controller,
          autofocus: true,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.message, color: AppColors.primary),
            border: OutlineInputBorder(),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.primary, width: 1.0),
            ),
          ),
          onChanged: (value) {
            print(value);
          },
        ),
      ],
    );
  }
}
