import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tabs/constants/colors.dart';

import 'package:tabs/routes.dart';
import 'package:tabs/screens/signin.dart';

import 'screens/home/home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          // primaryColor: AppColors.primary,
          textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(primary: AppColors.black)),
          scaffoldBackgroundColor: AppColors.white,
          outlinedButtonTheme: OutlinedButtonThemeData(
              style: OutlinedButton.styleFrom(
            backgroundColor: AppColors.primary,
            primary: AppColors.white,
          )),
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(primary: AppColors.primary)),
          floatingActionButtonTheme: const FloatingActionButtonThemeData(
              backgroundColor: AppColors.primary),
          appBarTheme: const AppBarTheme(backgroundColor: AppColors.primary),
          textTheme: const TextTheme(
              headline1: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w800,
                  color: AppColors.black),
              subtitle1: TextStyle(fontSize: 13, color: AppColors.grey100))),
      onGenerateRoute: AppRoutes.onGenerateRoute,
      home: const Home(),
      // home: const SignIn(),
    );
  }
}
