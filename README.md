# tabs

A new Flutter project.

## commands

` flutter pub run build_runner build`: generate JSON serialization code for your models

## Getting Started

A few resources to get you started if this is your first Flutter project:

- [JSON and serialization](https://flutter.dev/docs/development/data-and-backend/json)
- [Flutter State Management with Riverpod: The Essential Guide](https://codewithandrea.com/videos/flutter-state-management-riverpod/)
- [Illustrations](https://www.figma.com/community/file/883778082594341562)
